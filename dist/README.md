# dallmo-rss-filter

# overview
- a simple rss filter based on [rss-parse][link_1], filter out items from the parsed feed with keywords ; 
- each filter is domain-specific ; 

# list of filter and the corresponding feed
- hko : https://rss.weather.gov.hk/rss/WeatherWarningSummaryv2_uc.xml

[link_1]: https://www.npmjs.com/package/rss-parser

# usage

- requires a config file in `yarml`, which contains :
  - `feed_url` : the rss feed url ;
  - `keywords` : the list of keywords ;

## sample config file
```
---
# the RSS feed published by the Hongkong Obversatory
# traditional chinese
feed_url: https://rss.weather.gov.hk/rss/WeatherWarningSummaryv2_uc.xml

# the list of keywords to watch for
keywords:
  - "黑色暴雨"
  - "黃色暴雨"
  - "紅色暴雨"
  - "並無警告"
  - "一號"
  - "三號"
  - "八號"
  - "九號"
  - "十號"
```

## esm
```
"use strict";

import { hko as hko_rss } from "@dallmo/rss-filter/hko"

const config_file = "./etc/config.yaml";
const array_filtered_items = await hko_rss.filtered_items( config_file );
      console.log( "array_filtered_items : ", array_filtered_items );
```

## cjs
```
"use strict";

( async()=>{

  const hko_rss = await import("@dallmo/rss-filter/hko");

  // the rest are the same as those in esm
  // given we are inside an async function to await
  const config_file = "./etc/config.yaml";
  const array_filtered_items = await hko_rss.filtered_items( config_file );
      console.log( "array_filtered_items : ", array_filtered_items );  

})(); // self-run async main
```
