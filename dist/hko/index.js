"use strict";

/**
 * @dallmo/rss-filter/hko
 */

import {dallmo_yaml} from "dallmo-yaml";
import Parser from 'rss-parser';

/////////////////////////////////////////////////////
async function get_items_array( config_obj ){

  /**
   * private function
   * 
   * this function get the rss feed parsed with
   * the package rss-parser, in json format.
   */
  const feed_url = config_obj.feed_url;
  const parser = new Parser();

  // parsed from http-xml to json
  const feed_parsed = await parser.parseURL( feed_url );
  
    return feed_parsed.items;

}; // function get_feed_in_json
/////////////////////////////////////////////////////
function get_filtered_items( config_obj, array_feed_parsed_items ){

  /**
   * private function
   * 
   * this function filter the array of items
   * by the array of keywords defined in the config file
   */
  const array_keywords = config_obj.keywords;

  //...............................................
  // this will contain filtered items to return  
  let array_filtered_items = [];

      // check each item in "feed_parsed" with the keywords defined in config.yaml
      // only keep items with keywords found in title, and push them to this array
      array_filtered_items = array_feed_parsed_items.filter(
        each_feed_item =>{
          const this_feed_title = each_feed_item.title;
          // official ref of Array.prototype.some() : https://mzl.la/3RaZOiE
          if( array_keywords.some( keyword => this_feed_title.includes( keyword ) ) )
          { return each_feed_item; };
        }); // array filter
  //...............................................

  return array_filtered_items;

}; // function get_filtered_items
/////////////////////////////////////////////////////
async function filtered_items( config_file ){

  /**
   * public / exposed function
   * 
   * overall workflow : 
   * 1. get array of items downloaded and parsed by rss-parser ( from xml to json )
   * 2. filter the array from (1) according to the keywords defined in config file
   */

  // read config file provided by the user
  // read this only once and pass it to different functions 
  const config_obj = await dallmo_yaml( config_file );
  //console.log("inside module : config_obj : ", config_obj );

  const array_feed_parsed_items = await get_items_array( config_obj );
  //console.log("inside module : array of parsed items : ", array_feed_parsed_items );

  // this will contain filtered items to return  
  const array_filtered_items = 
        get_filtered_items( config_obj, array_feed_parsed_items );

    return array_filtered_items;

}; // function filtered_items
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/rss-filter/hko");

}; // function tester
/////////////////////////////////////////////////////
const hko = {

  tester,
  filtered_items,

}; // math
/////////////////////////////////////////////////////
export {
  
  filtered_items,
  tester,
  hko,

}; // export