{
  items: [
    {
      creator: '香港天文台',
      title: '發出山泥傾瀉警告 (23:45 HKT 07/09/2023) ',
      link: 'http://www.weather.gov.hk/textonly/warning/warnc.htm',
      pubDate: 'Thu, 07 Sep 2023 23:45:00 +0800',
      author: '香港天文台',
      content: '山泥傾瀉警告在23時45分發出( 2023年9月7日 )。 <br/><br/>',
      contentSnippet: '山泥傾瀉警告在23時45分發出( 2023年9月7日 )。',
      guid: 'http://rss.weather.gov.hk/rss/202309072345/發出山泥傾瀉警告',
      isoDate: '2023-09-07T15:45:00.000Z'
    },
    {
      creator: '香港天文台',
      title: '發出黑色暴雨警告信號 (23:05 HKT 07/09/2023) ',
      link: 'http://www.weather.gov.hk/textonly/warning/warnc.htm',
      pubDate: 'Thu, 07 Sep 2023 23:05:00 +0800',
      author: '香港天文台',
      content: '黑色暴雨警告信號在23時05分發出( 2023年9月7日 )。 <br/><br/>',
      contentSnippet: '黑色暴雨警告信號在23時05分發出( 2023年9月7日 )。',
      guid: 'http://rss.weather.gov.hk/rss/202309072305/發出黑色暴雨警告信號',
      isoDate: '2023-09-07T15:05:00.000Z'
    },
    {
      creator: '香港天文台',
      title: '發出新界北部水浸特別報告 (19:50 HKT 07/09/2023) ',
      link: 'http://www.weather.gov.hk/textonly/warning/warnc.htm',
      pubDate: 'Thu, 07 Sep 2023 19:50:00 +0800',
      author: '香港天文台',
      content: '新界北部水浸特別報告在19時50分發出( 2023年9月7日 )。 <br/><br/>',
      contentSnippet: '新界北部水浸特別報告在19時50分發出( 2023年9月7日 )。',
      guid: 'http://rss.weather.gov.hk/rss/202309071950/發出新界北部水浸特別報告',
      isoDate: '2023-09-07T11:50:00.000Z'
    },
    {
      creator: '香港天文台',
      title: '發出雷暴警告 (14:30 HKT 07/09/2023) ',
      link: 'http://www.weather.gov.hk/textonly/warning/warnc.htm',
      pubDate: 'Thu, 07 Sep 2023 14:30:00 +0800',
      author: '香港天文台',
      content: '雷暴警告在14時30分發出( 2023年9月7日 )。 <br/><br/>',
      contentSnippet: '雷暴警告在14時30分發出( 2023年9月7日 )。',
      guid: 'http://rss.weather.gov.hk/rss/202309071430/發出雷暴警告',
      isoDate: '2023-09-07T06:30:00.000Z'
    }
  ],
  image: {
    url: 'http://rss.weather.gov.hk/img/logo_dblue.gif',
    title: '香港天文台',
    width: '333',
    height: '65'
  },
  title: '天氣警告一覽',
  description: '由香港天文台提供的天氣警告一覽',
  pubDate: 'Fri, 08 Sep 2023 10:30:58 +0800',
  webMaster: 'mailbox@hko.gov.hk',
  link: 'http://www.weather.gov.hk/textonly/warning/warnc.htm',
  language: 'zh-tw',
  copyright: '本檔案的內容，包括但不限於所有文本、平面圖像、圖畫、圖片、照片以及數據或其他資料的匯編，均受版權保障。香港特別行政區政府是本檔案內所有版權作品的擁有人。'
}