"use strict";

( async()=>{

  const hko_rss = await import("@dallmo/rss-filter/hko");
        hko_rss.tester();

  // the rest are the same as those in esm
  // given we are inside an async function to await
  const config_file = "./etc/config.yaml";
  const array_filtered_items = await hko_rss.filtered_items( config_file );
      console.log( "array_filtered_items : ", array_filtered_items );  

})(); // self-run async main
